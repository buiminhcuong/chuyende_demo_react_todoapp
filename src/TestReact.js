import React from 'react';

class TestReact extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            name: props.name
        }
    }
    handleChange(e) {
        var state = {
            name: e.target.value
        }
        this.setState(state);
    }

    render() {
        return (<div>
                <input type="text" onChange={(e) => this.handleChange(e)} value={this.state.name}/>
                <span>{this.state.name}</span>
            </div>);   
    }
}

export default TestReact