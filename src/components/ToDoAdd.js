import React from "react";

var ToDoAdd = function (props) {
  var inputRef = React.createRef();
  var handleAdd = function () {
    props.onAdd(inputRef.current.value);
    inputRef.current.value = "";
  };
  return (
    <div>
      Add: <input ref={inputRef} name="todo_add" />
      <input type="button" value="Add" onClick={() => handleAdd()} />
    </div>
  );
};

export default ToDoAdd;
