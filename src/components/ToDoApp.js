import React from "react";
import ToDoFilter from "./ToDoFilter";
import ToDoList from "./ToDoList";
import ToDoAdd from "./ToDoAdd";
class ToDoApp extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      filter: "",
      currentId: 3,
      todos: [
        { id: 1, text: "todo1" },
        { id: 2, text: "todo2" },
        { id: 3, text: "todo3" },
      ],
    };
  }
  getToDo() {
    return this.state.todos.filter((item) => {
      var filterText = this.state.filter;
      return !filterText || item.text.includes(filterText);
    });
  }
  handleFilter(filter) {
    this.setState({ ...this.state, filter });
  }
  handleAdd(value) {
    var newState = { ...this.state };
    newState.currentId++;
    newState.todos.push({
      id: newState.currentId,
      text: value,
    });
    this.setState(newState);
  }
  handleDelete(id) {
      var newState = {...this.state, todos: this.state.todos.filter(item => item.id !== id)}
      this.setState(newState);
  }
  render() {
    return (
      <div>
        <ToDoFilter
          filter={this.state.filter}
          onFilter={(filter) => this.handleFilter(filter)}
        />
        <ToDoAdd onAdd={(value) => this.handleAdd(value)} />
        <ToDoList todos={this.getToDo()} onDelete={(id) => this.handleDelete(id)} />
      </div>
    );
  }
}
export default ToDoApp;
