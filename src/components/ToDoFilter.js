import React from "react";

var ToDoFilter = function(props) {
    return <div>
            Filter: <input name="filter" value={props.filter} 
                        onChange={(e) => props.onFilter(e.target.value)}/>
        </div>
}

export default ToDoFilter;