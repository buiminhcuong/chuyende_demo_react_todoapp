import React from "react";
var ToDoItem = function(props) {
    var handleDelete = function() {
        props.onDelete(props.item.id);
    }
    return <li>{props.item.text} 
        <input type="button" value="X" onClick={()=>handleDelete()} /></li>
}
var ToDoList = function(props) {
    var todoItems = props.todos.map(todo => {
        return (<ToDoItem onDelete={(id) => props.onDelete(id)} item={todo} key={todo.id} />);
    });

    return <ul>
        {todoItems}
    </ul>
}
export default ToDoList